# EGS_BANK_SERVICE

## Introduction

This spring-boot project is responsible for handling all the operations related to BANK Simulator. these operations
include:

* deposit/withdraw/check balance
* check card password and expiration date

## Application Properties

All important and critical data must be placed in "SECRET_FILE" with the same name of image-name in kubernetes
environment.

|Property Name|Default Value|Mapped Environment Variable|Description|
|-------------|-------------|---------------------------|-----------|
|spring.profiles.active|-|ACTIVE_PROFILE|it's use in development mode. you can set 'dev' in development|
|CI-FILES|-|SIT_UAT_PROD|you can use 'sit','uat','prod' in order to deploy in different environment|
|spring.application.name|egs-bank-service|SPRING_APPLICATION_NAME|-|
|spring.image.name|-|IMAGE_NAME|the image-name string logged in the logstash entries. this will be injected during the ci/cd operations|
|spring.logstash.server|-|LOGSTASH_SERVER|logstash server address|
|spring.logstash.port|-|LOGSTASH_PORT|logstash server port|
|spring.datasource.type|com.zaxxer.hikari.HikariDataSource|-||
|spring.datasource.url|-|DATABASE_URL|jdbc connection string|
|spring.datasource.username|-|DATABASE_USERNAME|username to connect to db|
|spring.datasource.password|-|DATABASE_PASSWORD|password to connect to db|
|spring.datasource.jpa.database-platform|org.hibernate.dialect.MySQL8Dialect|-|-|
|spring.datasource.jpa.database|MYSQL|-|-|
|spring.datasource.jpa.show-sql|-|SHOW_SQL|for debugging purposes, set to true|
|spring.datasource.hikari.maximum-pool-size|-|MAX_POOL_SIZE|maximum number of connections in hikari connection pool|
|spring.liquibase.change-log|classpath:master.xml|-|address to liquibase's master file|
|spring.jpa.open-in-view|false|-||
|server.port|-|SERVER_PORT|server port|
|logging.level.ROOT|-|LOG_LEVEL|defines the root log level. suggested value: `INFO`|
|management.server.port|-|MANAGEMENT_SERVER_PORT|defines the management server port. health and metrics APIs will be available in this port|
|security.sessions|stateless|-|-|
|security.user.password|basic username,password|-|-|
