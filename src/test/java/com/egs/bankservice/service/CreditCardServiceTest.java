package com.egs.bankservice.service;

import com.egs.bankservice.dao.domain.CreditCard;
import com.egs.bankservice.dao.domain.enums.PreferAuthentication;
import com.egs.bankservice.dao.repository.CreditCardRepository;
import com.egs.bankservice.dto.PreferAuthMethodDto;
import com.egs.bankservice.dto.TransferDto;
import com.egs.bankservice.exception.CardBalanceIsNotEnoughException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.util.Optional;
import java.util.UUID;

import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
public class CreditCardServiceTest {
    private final String cardNumber = UUID.randomUUID().toString();
    private final String password = UUID.randomUUID().toString();
    private final Double amount = Double.valueOf(1000);

    @Mock
    private CreditCardRepository creditCardRepository;
    @InjectMocks
    private CreditCardServiceImp creditCardService;


    @Test
    void getCreditCardBalance() {

        doReturn(Optional.of(new CreditCard()))
                .when(creditCardRepository)
                .findByCardNumber(cardNumber);

        creditCardService.getCreditCardBalance(cardNumber);

        verify(creditCardRepository).findByCardNumber(cardNumber);
    }

    @Test
    void checkPassword() throws Exception {

        CreditCard creditCard = new CreditCard();

        creditCard.setPreferAuthentication(PreferAuthentication.PIN);
        creditCard.setPin("1234");
        creditCard.setExpirationDate(LocalDate.now().plusDays(5));
        creditCard.setNumberOfFailedAttempt(0);

        doReturn(Optional.of(creditCard))
                .when(creditCardRepository)
                .findByCardNumber(cardNumber);

        creditCardService.checkPassword(cardNumber, password);

        verify(creditCardRepository).findByCardNumber(cardNumber);
    }

    @Test
    void deposit() {

        doReturn(Optional.of(new CreditCard()))
                .when(creditCardRepository)
                .findByCardNumber(cardNumber);

        creditCardService.deposit(cardNumber, new TransferDto(amount));

        verify(creditCardRepository).findByCardNumber(cardNumber);
    }

    @Test
    void withdraw() throws CardBalanceIsNotEnoughException {
        CreditCard creditCard = new CreditCard();
        creditCard.setBalance(2000);
        doReturn(Optional.of(creditCard))
                .when(creditCardRepository)
                .findByCardNumber(cardNumber);

        creditCardService.withdraw(cardNumber, new TransferDto(amount));

        verify(creditCardRepository).findByCardNumber(cardNumber);
    }
    @Test
    void setPreferAuthMethod() {
        PreferAuthMethodDto authMethodDto = new PreferAuthMethodDto(PreferAuthentication.PIN);

        doReturn(Optional.of(new CreditCard()))
                .when(creditCardRepository)
                .findByCardNumber(cardNumber);

        creditCardService.setPreferAuthMethod(cardNumber, authMethodDto);

        verify(creditCardRepository).findByCardNumber(cardNumber);
    }
}
