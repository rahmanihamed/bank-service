package com.egs.bankservice.service;

import com.egs.bankservice.dto.PreferAuthMethodDto;
import com.egs.bankservice.dto.TransferDto;
import com.egs.bankservice.exception.CardBalanceIsNotEnoughException;

public interface CreditCardService {

    Double getCreditCardBalance(String cardNumber);

    void deposit(String cardNumber, TransferDto transferDto);

    void withdraw(String cardNumber, TransferDto transferDto) throws CardBalanceIsNotEnoughException;

    boolean checkPassword(String cardNumber, String password) throws Exception;

    void setPreferAuthMethod(String cardNumber, PreferAuthMethodDto preferAuthMethodDto);
}
