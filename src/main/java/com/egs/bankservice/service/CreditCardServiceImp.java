package com.egs.bankservice.service;

import com.egs.bankservice.dao.domain.CreditCard;
import com.egs.bankservice.dao.domain.Transaction;
import com.egs.bankservice.dao.domain.enums.TransactionStatus;
import com.egs.bankservice.dao.domain.enums.TransactionType;
import com.egs.bankservice.dao.repository.CreditCardRepository;
import com.egs.bankservice.dao.repository.TransactionRepository;
import com.egs.bankservice.dto.PreferAuthMethodDto;
import com.egs.bankservice.dto.TransferDto;
import com.egs.bankservice.exception.CardBalanceIsNotEnoughException;
import com.egs.bankservice.exception.CardIsBlockedOrExpiredException;
import com.egs.bankservice.exception.NotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Service
@RequiredArgsConstructor
public class CreditCardServiceImp implements CreditCardService {

    private final CreditCardRepository creditCardRepository;
    private final TransactionRepository transactionRepository;

    @Override
    public Double getCreditCardBalance(String cardNumber) {
        CreditCard creditCard = getCreditCard(cardNumber);
        return creditCard.getBalance();
    }

    @Override
    @Transactional
    public void deposit(String cardNumber, TransferDto transferDto) {
        CreditCard creditCard = getCreditCard(cardNumber);
        creditCard.setBalance(creditCard.getBalance() + transferDto.getAmount());
        creditCardRepository.save(creditCard);
    }

    @Override
    @Transactional
    public void withdraw(String cardNumber, TransferDto transferDto) throws CardBalanceIsNotEnoughException {
        CreditCard creditCard = getCreditCard(cardNumber);
        checkCardBalance(transferDto, creditCard);
        creditCard.setBalance(creditCard.getBalance() - transferDto.getAmount());
        creditCardRepository.save(creditCard);
    }

    private void checkCardBalance(TransferDto transferDto, CreditCard creditCard) throws CardBalanceIsNotEnoughException {
        if (transferDto.getAmount() > creditCard.getBalance())
            throw new CardBalanceIsNotEnoughException();
    }

    @Override
    public boolean checkPassword(String cardNumber, String password) throws Exception {

        CreditCard creditCard = getCreditCard(cardNumber);

        checkIsBlockedCard(creditCard);

        checkIsExpired(creditCard);

        return checkPass(creditCard, password);

    }

    private void checkIsExpired(CreditCard creditCard) throws CardIsBlockedOrExpiredException {
        if (creditCard.getExpirationDate().atStartOfDay().isBefore(LocalDate.now().atStartOfDay()))
            throw new CardIsBlockedOrExpiredException();
    }

    @Override
    public void setPreferAuthMethod(String cardNumber, PreferAuthMethodDto preferAuthMethodDto) {
        CreditCard creditCard = getCreditCard(cardNumber);
        creditCard.setPreferAuthentication(preferAuthMethodDto.getPreferAuthentication());
        creditCardRepository.save(creditCard);
    }

    private void checkIsBlockedCard(CreditCard creditCard) throws CardIsBlockedOrExpiredException {
        if (creditCard.isBlocked())
            throw new CardIsBlockedOrExpiredException();
    }

    private boolean checkPass(CreditCard creditCard, String password) throws CardIsBlockedOrExpiredException {
        boolean result;
        switch (creditCard.getPreferAuthentication()) {
            case PIN:
                result = creditCard.getPin().equals(password);
                break;
            case FINGERPRINT:
                result = creditCard.getPassword().equals(password);
                break;
            default:
                result = false;

        }
        setPasswordStatus(creditCard, result);
        return result;
    }

    private void setPasswordStatus(CreditCard creditCard, boolean result) throws CardIsBlockedOrExpiredException {
        if (!result) {
            creditCard.setNumberOfFailedAttempt(creditCard.getNumberOfFailedAttempt() + 1);
            if (creditCard.getNumberOfFailedAttempt() >= 3) {
                creditCard.setBlocked(true);
                throw new CardIsBlockedOrExpiredException();
            }

        }
        if (result && creditCard.getNumberOfFailedAttempt() > 0) {
            creditCard.setNumberOfFailedAttempt(0);
            creditCardRepository.save(creditCard);
        }
    }

    private CreditCard getCreditCard(String cardNumber) {
        return creditCardRepository.findByCardNumber(cardNumber)
                .orElseThrow(NotFoundException::new);
    }
}
