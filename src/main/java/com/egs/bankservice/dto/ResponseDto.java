package com.egs.bankservice.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResponseDto<T> {
    private boolean isSuccess;
    private String errorMessage;
    private T data;

    public ResponseDto success(T data) {
        this.data = data;
        this.isSuccess = true;
        return this;
    }

    public ResponseDto success() {
        this.isSuccess = true;
        return this;
    }

    public ResponseDto failed(String errorMessage) {
        this.errorMessage = errorMessage;
        this.isSuccess = false;
        return this;
    }
}
