package com.egs.bankservice.dto;

import com.egs.bankservice.dao.domain.enums.PreferAuthentication;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PreferAuthMethodDto {

    @NotNull
    private PreferAuthentication preferAuthentication;
}
