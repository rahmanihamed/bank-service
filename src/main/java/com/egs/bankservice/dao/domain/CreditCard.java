package com.egs.bankservice.dao.domain;

import com.egs.bankservice.dao.domain.enums.PreferAuthentication;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table( name = "CREDIT_CARD")
public class CreditCard {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", nullable = false)
    private Integer id;

    @Column(name = "CARD_NUMBER", nullable = false)
    private String cardNumber;

    @Column(name = "BALANCE")
    private double balance;

    @Column(name = "CREATION_DATE")
    private LocalDate creationDate;

    @Column(name = "EXPIRATION_DATE")
    private LocalDate expirationDate;

    @Column(name = "PASSWORD")
    private String password;

    @Column(name = "PIN")
    private String pin;

    @Column(name = "NUMBER_OF_FAILED_ATTEMPT")
    private Integer numberOfFailedAttempt;

    @Column(name = "IS_BLOCKED")
    private boolean isBlocked;

    @Column(name = "PREFER_AUTHENTICATION", nullable = false)
    @Enumerated(EnumType.STRING)
    private PreferAuthentication preferAuthentication;
}
