package com.egs.bankservice.dao.domain.enums;

public enum PreferAuthentication {
    PIN, FINGERPRINT
}
