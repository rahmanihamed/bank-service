package com.egs.bankservice.dao.domain.enums;

public enum TransactionStatus {
    SUCCESS, FAILED
}
