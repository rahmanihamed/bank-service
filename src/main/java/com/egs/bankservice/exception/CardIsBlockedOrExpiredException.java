package com.egs.bankservice.exception;

public class CardIsBlockedOrExpiredException extends Exception {
    public CardIsBlockedOrExpiredException() {
        super("Credit Card is Blocked or Expired.");
    }
}
