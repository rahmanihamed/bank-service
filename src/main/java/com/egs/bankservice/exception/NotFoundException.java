package com.egs.bankservice.exception;

import javax.persistence.EntityNotFoundException;

public class NotFoundException extends EntityNotFoundException {
    public NotFoundException() {
        super("Credit Card Number is not found.");
    }
}
