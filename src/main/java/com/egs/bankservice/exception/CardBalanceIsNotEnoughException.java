package com.egs.bankservice.exception;

public class CardBalanceIsNotEnoughException extends Exception {
    public CardBalanceIsNotEnoughException() {
        super("Withdraw amount can't be more than Card balance.");
    }
}
