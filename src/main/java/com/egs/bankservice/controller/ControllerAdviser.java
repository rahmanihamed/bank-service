package com.egs.bankservice.controller;

import com.egs.bankservice.dto.ResponseDto;
import com.egs.bankservice.exception.CardBalanceIsNotEnoughException;
import com.egs.bankservice.exception.CardIsBlockedOrExpiredException;
import io.github.resilience4j.ratelimiter.RequestNotPermitted;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import javax.persistence.EntityNotFoundException;
import java.util.UUID;
import java.util.function.Function;
import java.util.stream.Collectors;

@Slf4j
@ControllerAdvice
public class ControllerAdviser {
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<ResponseDto> echoResponseMessage(MethodArgumentNotValidException exception) {
        log.warn("received bad request.", exception);
        String message = exception.getBindingResult().getAllErrors().stream()
                .map(FIELD_ERROR_PROCESSOR)
                .collect(Collectors.joining(";"));
        return ResponseEntity
                .badRequest()
                .body(new ResponseDto().failed(message));
    }

    @ExceptionHandler(ConstraintViolationException.class)
    public ResponseEntity<ResponseDto> echoResponseMessage(ConstraintViolationException exception) {
        String message = exception.getMessage();
        return ResponseEntity
                .badRequest()
                .body(new ResponseDto().failed(message));
    }

    //for inputs of type LocalDate
    @ExceptionHandler(MethodArgumentTypeMismatchException.class)
    public ResponseEntity<ResponseDto> echoResponseMessage(MethodArgumentTypeMismatchException exception) {
        String message = "value for '".concat(exception.getParameter().getParameterName()).concat("' is not valid: ").concat(exception.getMostSpecificCause().getMessage());
        return ResponseEntity
                .badRequest()
                .body(new ResponseDto().failed(message));
    }


    @ExceptionHandler(MissingServletRequestParameterException.class)
    public ResponseEntity<ResponseDto> echoResponseMessage(MissingServletRequestParameterException exception) {
        String message = "value for request-param '".concat(exception.getParameterName()).concat("' is missing");
        return ResponseEntity
                .badRequest()
                .body(new ResponseDto().failed(message));
    }

    @ExceptionHandler(BindException.class)
    public ResponseEntity<ResponseDto> handleBindException(BindException exception) {
        String errorReference = UUID.randomUUID().toString();
        exception.getFieldErrors().forEach(m ->
                echoResponseMessage(new MissingServletRequestParameterException(m.getField(), m.getField()))
        );
        if (!exception.getBindingResult().getAllErrors().isEmpty()) { // handle MethodArgumentNotValidException
            log.warn("received a bad request", exception);
            String message = exception.getBindingResult().getAllErrors().stream()
                    .map(FIELD_ERROR_PROCESSOR)
                    .collect(Collectors.joining(";"));
            return ResponseEntity
                    .badRequest()
                    .body(new ResponseDto().failed(message));
        }

        log.warn("Caught exception, reference: '{}'", errorReference, exception);
        String message = "value for request-param '".concat(exception.getMessage()).concat("' is missing");
        return ResponseEntity
                .badRequest()
                .body(new ResponseDto().failed(message));
    }

    @ExceptionHandler(CardIsBlockedOrExpiredException.class)
    public ResponseEntity<ResponseDto> handleEntityNotFoundException(CardIsBlockedOrExpiredException exception) {
        return ResponseEntity
                .status(HttpStatus.FORBIDDEN)
                .body(new ResponseDto().failed(exception.getMessage()));
    }

    @ExceptionHandler(CardBalanceIsNotEnoughException.class)
    public ResponseEntity<ResponseDto> handleCardBalanceIsNotEnoughException(CardBalanceIsNotEnoughException exception) {
        return ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .body(new ResponseDto().failed(exception.getMessage()));
    }

    @ExceptionHandler(EntityNotFoundException.class)
    public ResponseEntity<ResponseDto> handleEntityNotFoundException(EntityNotFoundException exception) {
        return ResponseEntity
                .status(HttpStatus.NOT_FOUND)
                .body(new ResponseDto().failed(exception.getMessage()));
    }

    @ExceptionHandler(RequestNotPermitted.class)
    public ResponseEntity<ResponseDto> handleRequestNotPermittedException(RequestNotPermitted exception) {
        return ResponseEntity
                .status(HttpStatus.TOO_MANY_REQUESTS)
                .body(new ResponseDto().failed("RateLimiter 'Bank service' does not permit further calls."));
    }


    @ExceptionHandler(Exception.class)
    public ResponseEntity<ResponseDto> handleUnknownException(Exception exception) {
        String errorReference = UUID.randomUUID().toString();
        log.warn("Caught exception, reference: '{}'", errorReference, exception);
        String message = "Internal error in Bank_Service occurred. You can trace this in logs using reference: '".concat(errorReference).concat("'");
        return ResponseEntity
                .status(HttpStatus.INTERNAL_SERVER_ERROR)
                .body(new ResponseDto().failed(message));
    }

    private static final Function<ObjectError, String> FIELD_ERROR_PROCESSOR = error -> {
        if (error instanceof FieldError) {
            FieldError fieldError = (FieldError) error;
            return fieldError.getField().concat(" has invalid value: ").concat(fieldError.getDefaultMessage());
        }
        return error.getDefaultMessage();
    };
}
