package com.egs.bankservice.controller;

import com.egs.bankservice.dto.PreferAuthMethodDto;
import com.egs.bankservice.dto.ResponseDto;
import com.egs.bankservice.dto.TransferDto;
import com.egs.bankservice.exception.CardBalanceIsNotEnoughException;
import com.egs.bankservice.service.CreditCardService;
import io.github.resilience4j.ratelimiter.annotation.RateLimiter;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;

@RestController
@RequestMapping("/api/v1/credit-card")
@AllArgsConstructor
public class CreditCardController {

    private static final String BANK_SERVICE = "default";
    private final CreditCardService creditCardService;

    @GetMapping("/{cardNumber}/get-balance")
    @RateLimiter(name = BANK_SERVICE)
    public ResponseEntity<ResponseDto> getCreditCardBalance(@PathVariable String cardNumber) {

        return ResponseEntity.ok(new ResponseDto().success(creditCardService.getCreditCardBalance(cardNumber)));
    }

    @GetMapping("/{cardNumber}/check-password")
    @RateLimiter(name = BANK_SERVICE)
    public ResponseEntity<ResponseDto> checkPassword(@PathVariable String cardNumber, @RequestParam @NotBlank String password) throws Exception {

        return ResponseEntity.ok(new ResponseDto().success(creditCardService.checkPassword(cardNumber, password)));
    }

    @PutMapping("/{cardNumber}/deposit")
    @RateLimiter(name = BANK_SERVICE)
    public ResponseEntity<ResponseDto> deposit(@PathVariable String cardNumber, @RequestBody @Valid TransferDto transferDto) {

        creditCardService.deposit(cardNumber, transferDto);
        return ResponseEntity.ok(new ResponseDto().success());
    }

    @PutMapping("/{cardNumber}/withdraw")
    @RateLimiter(name = BANK_SERVICE)
    public ResponseEntity<ResponseDto> withdraw(@PathVariable String cardNumber, @RequestBody @Valid TransferDto transferDto) throws CardBalanceIsNotEnoughException {

        creditCardService.withdraw(cardNumber, transferDto);
        return ResponseEntity.ok(new ResponseDto().success());
    }

    @PutMapping("/{cardNumber}/prefer-auth")
    @RateLimiter(name = BANK_SERVICE)
    public ResponseEntity<ResponseDto> setPreferAuthMethod(@PathVariable String cardNumber, @RequestBody @Valid PreferAuthMethodDto preferAuthMethodDto) {

        creditCardService.setPreferAuthMethod(cardNumber, preferAuthMethodDto);
        return ResponseEntity.ok(new ResponseDto().success());
    }

}
