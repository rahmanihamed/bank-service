create table test.credit_card
(
    ID                       INT         not null primary key,
    BALANCE                  DECIMAL,
    CARD_NUMBER              NVARCHAR(16) not null,
    CREATION_DATE            TIMESTAMP(6)      not null,
    EXPIRATION_DATE          TIMESTAMP(6),
    NUMBER_OF_FAILED_ATTEMPT INT     default 0 not null,
    IS_BLOCKED               BOOLEAN default 0 not null,
    PASSWORD                 NVARCHAR(64),
    PIN                      NVARCHAR(4) not null,
    PREFER_AUTHENTICATION    NVARCHAR(30) default 'PIN' not null
);
create table transaction_log
(
    ID                 INT      not null primary key,
    BALANCE            DECIMAL not null,
    CREATION_DATE_TIME TIMESTAMP(6) null,
    STATUS             NVARCHAR(64) not null,
    TYPE               NVARCHAR(64) not null
);

INSERT INTO test.credit_card
values (1,
        10000,
        1234567891234567,
        NOW(),
        '2022-02-02',
        0,
        0,
        '123456789',
        '1234',
        'PIN');
INSERT INTO test.credit_card
values (2,
        20000,
        1234567891234566,
        NOW(),
        '2022-03-03',
        0,
        0,
        '987654321',
        '1234',
        'FINGERPRINT');